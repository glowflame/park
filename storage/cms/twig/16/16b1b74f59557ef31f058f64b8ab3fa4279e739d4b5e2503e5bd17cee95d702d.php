<?php

/* /Users/nikita/WebstormProjects/park/themes/new-design/pages/attractions-price-list.htm */
class __TwigTemplate_49763358a88d999f5661c8fcbdcb3c611a72ee93ae723597620e85af15d72846 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"page-wrapper\">
    <div class=\"standard-block\">
    \t
    \t";
        // line 4
        $context["records"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "records", array());
        // line 5
        $context["displayColumn"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "displayColumn", array());
        // line 6
        $context["noRecordsMessage"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "noRecordsMessage", array());
        // line 7
        $context["detailsPage"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "detailsPage", array());
        // line 8
        $context["detailsKeyColumn"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "detailsKeyColumn", array());
        // line 9
        $context["detailsUrlParameter"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "detailsUrlParameter", array());
        // line 10
        echo "
        <table class=\"attractions-price__table\">
            <tr class=\"attractions-price__table__title text-center\">

                <td rowspan=\"2\">№</td>
                <td rowspan=\"2\">Наименование аттракциона</td>
                <td rowspan=\"2\">Тип аттракциона</td>
                <td colspan=\"2\">Цена билета</td>
                <td rowspan=\"2\">Режим работы</td>

            </tr>

            <tr>
                <td class=\" text-center\">Будние дни</td>
                <td class=\" text-center\">Выходные дни</td>

            </tr>

            ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 29
            echo "            <tr>
                <td class=\"attractions-price__table__title text-center\">";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "id", array()), "html", null, true);
            echo "</td>
                <td class=\"attractions-price__table__name\">";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "title", array()), "html", null, true);
            echo "</td>
                <td class=\"attractions-price__table__name\">";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "type", array()), "html", null, true);
            echo "</td>
                <td class=\"attractions-price__table__price text-center\">";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "weekdays", array()), "html", null, true);
            echo " руб.</td>
                <td class=\" text-center attractions-price__table__price\">";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "weekend", array()), "html", null, true);
            echo " руб.</td>
                <td class=\"attractions-price__table__title text-center\">";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "hours", array()), "html", null, true);
            echo "</td>
            </tr>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "

        </table>



    </div>


</div>";
    }

    public function getTemplateName()
    {
        return "/Users/nikita/WebstormProjects/park/themes/new-design/pages/attractions-price-list.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 39,  83 => 35,  79 => 34,  75 => 33,  71 => 32,  67 => 31,  63 => 30,  60 => 29,  56 => 28,  36 => 10,  34 => 9,  32 => 8,  30 => 7,  28 => 6,  26 => 5,  24 => 4,  19 => 1,);
    }
}
/* <div class="page-wrapper">*/
/*     <div class="standard-block">*/
/*     	*/
/*     	{% set records = builderList.records %}*/
/* {% set displayColumn = builderList.displayColumn %}*/
/* {% set noRecordsMessage = builderList.noRecordsMessage %}*/
/* {% set detailsPage = builderList.detailsPage %}*/
/* {% set detailsKeyColumn = builderList.detailsKeyColumn %}*/
/* {% set detailsUrlParameter = builderList.detailsUrlParameter %}*/
/* */
/*         <table class="attractions-price__table">*/
/*             <tr class="attractions-price__table__title text-center">*/
/* */
/*                 <td rowspan="2">№</td>*/
/*                 <td rowspan="2">Наименование аттракциона</td>*/
/*                 <td rowspan="2">Тип аттракциона</td>*/
/*                 <td colspan="2">Цена билета</td>*/
/*                 <td rowspan="2">Режим работы</td>*/
/* */
/*             </tr>*/
/* */
/*             <tr>*/
/*                 <td class=" text-center">Будние дни</td>*/
/*                 <td class=" text-center">Выходные дни</td>*/
/* */
/*             </tr>*/
/* */
/*             {% for record in records %}*/
/*             <tr>*/
/*                 <td class="attractions-price__table__title text-center">{{record.id}}</td>*/
/*                 <td class="attractions-price__table__name">{{record.title}}</td>*/
/*                 <td class="attractions-price__table__name">{{record.type}}</td>*/
/*                 <td class="attractions-price__table__price text-center">{{record.weekdays}} руб.</td>*/
/*                 <td class=" text-center attractions-price__table__price">{{record.weekend}} руб.</td>*/
/*                 <td class="attractions-price__table__title text-center">{{record.hours}}</td>*/
/*             </tr>*/
/* */
/*             {% endfor %}*/
/* */
/* */
/*         </table>*/
/* */
/* */
/* */
/*     </div>*/
/* */
/* */
/* </div>*/
