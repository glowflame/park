<?php

/* /Users/nikita/WebstormProjects/park/themes/new-design/pages/history.htm */
class __TwigTemplate_aaedc2638ba238cd17093c3b63c50d24bdd5ae55407c24ef934945c79aa9d819 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"page-href\" href=\"history\"></div>

";
        // line 3
        $context["records"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "records", array());
        // line 4
        $context["displayColumn"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "displayColumn", array());
        // line 5
        $context["noRecordsMessage"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "noRecordsMessage", array());
        // line 6
        $context["detailsPage"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "detailsPage", array());
        // line 7
        $context["detailsKeyColumn"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "detailsKeyColumn", array());
        // line 8
        $context["detailsUrlParameter"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "detailsUrlParameter", array());
        // line 9
        echo "<div class=\"page-wrapper\">
<div class=\"page__title text-align-center standard-block\">ИСТОРИЯ ПАРКА</div>


<div class=\"history-page-wrapper standard-block\">

    <p>Сначала на месте парка, напротив Управления СКЖД ближе к центру района находился огромный Софиевский собор с обширной Софиевской площадью. На площади проходили гуляния горожан. Тут же находилось и кладбище, где хоронили нахичеванских богачей. Ежегодно 23 апреля нахичеванские армяне в пестрых национальных костюмах заполняли площадь. Здесь проходили спортивные игры, выборы невест,можно было попробовать кулинарные изыски, приготовленные по семейным рецептам. Софиевский собор был разрушен в первые годы советской власти, посчитавшей его центром контрреволюции. Кладбище постигла та же участь, так как богатые усыпальницы живо напоминали о прошлой, достаточно благопристойной и сытой жизни. Сейчас территория бывших Софиевской площади и кладбища занята парком им. Октябрьской революции.</p>
    <p>Как культурная \"единица\" парк состоялся в 1926 году, площадь его составляла более 15 гектаров. Парк находился между городами Ростов-на-Дону и Нахичевань. В то время часть Пролетарского района Ростова-на-Дону была отдельным городом. В тот период  в Крымском ханстве армян стали убивать и притеснять, и они не смогли отстоять свои дома. Тогда армяне попросили помощи у русских и казаков, и те приютили их под стенами Ростовской крепости . Екатерина II  разрешила армянам- беженцам основать свой особый город Hop-Нахичевань, в результате того, что эти люди получили различные льготы и при этом не несли воинской повинности, город вскоре экономически расцвел и население быстро разрослось. Затем этот населённый пункт переименовали в Нахичевань-на-Дону , а в 1928 году его присоединили к Ростову-на-Дону.</p>
    <img class=\"history-page__photo\" src=\"/themes/new-design/assets/images/history/information_items_property_3639.jpg\">

    <p>В 1935 году было завершено строительство театра имени Горького, который является одним из шедевров мировой архитектуры﻿. В основу архитектурных форм здания была положена стилизация гусеничного трактора. Парк в плотную примыкает к театру. После постройки театра имени Горького, рядом с ним возвели красивейший фонтан  «Атланты».</p>
    <p>К 2000 году парк находился в полу-запущенном состоянии. Далеко не каждый житель Ростова-на-Дону решался зайти в него в темное время суток.</p>
    <p>Начиная с 2002 года парк Революции начали активно восстанавливать. В процессе реконструкции парка проводились работы по укладке новой тротуарной плитки, посадки деревьев и цветов, строительство детских площадок, установка аттракционов и памятников. За короткий промежуток времени парк Революции возродился вновь и стал очень популярен среди ростовчан и гостей нашего города как одно из самых лучших мест отдыха.</p>
    <p>Сегодня парк предоставляет своим посетителям разнообразные (в том числе бесплатные) аттракционы, зрелища, культурно-спортивные мероприятия. Огромная территория парка Революции засажена деревьями, кустарниками. Ко всему прочему в парке построено искусственное озеро для водоплавающих птиц, а также в птичниках содержатся павлины, журавли, лебеди и фламинго.</p>


    <img class=\"history-page__photo\" src=\"/themes/new-design/assets/images/history/lJ2mva89PKQ.jpg\">

    <p>На территории парка Революции есть спортивная зона. Активно отдохнуть можно на  катке \"Ледоград\". В парке есть  футбольное поле для мини-футбола и оборудована площадка для пейнтбола и экстремального лазанья по деревьям, а также площадка для настольного тенниса. На территории парка находится самая большая в Ростове-на-Дону воркаут площадка. На площадке ﻿ находится множество турников и перекладин, рукоходы и брусья, лестницы и скамьи для тренировки пресса. Все  оборудование современное и обеспечивает безопасные тренировки как детям, взрослым, так и лицам с ограниченными возможностями.</p>

    <p>В парке Революции установлен памятник Петра и Февронии, символизирующий семейное счастье, скамья примирения и другие интересные достопримечательности. Многие семейные пары приезжают на фотосессию в парк Революции.</p>
    <p>На территории парка есть много детских площадок, на которых детишки смогут поиграть, а родители отдохнуть в тенечке на лавочках, которые расположены на всех аллеях парка. И взрослым и детям будет весело и интересно в «Городке аттракционов», где можно найти развлечения на любой вкус. Там можно покататься на семейных, детских или экстремальных аттракционах.﻿</p>
    <p>На территории парка находится «Театр Папы Карло» ﻿- это первый в Ростове уличный кукольный театр. Представления в театре - бесплатные. Они рассчитаны на детей от 3-х лет. Репертуар театра постоянно обновляется.</p>


    <img class=\"history-page__photo\" src=\"/themes/new-design/assets/images/history/k3.jpg\">
    <p>Для любителей спокойного отдыха в парке есть зеленая зона, с ухоженными газонами и деревьями, на которых живут белочки. Белочки, уже привыкшие к людям, с нетерпением ждут угощений от посетителей парка. В парке вы сможете прокатиться на колесе обозрения. Его высота составляет 65 метров. ﻿Благодаря тому, что кабинки на колесе будут закрытыми, аттракцион будет работать не только летом, но и зимой. Желающим прокатиться на колесе, откроется прекрасный вид на реку Дон.</p>
    <p>На территории парка множество кафе и мест, где можно быстро и вкусно перекусить или утолить жажду. Одно из таких мест это ресторан «Ялла», в котором можно попробовать блюда восточной, европейской, итальянской, русской, узбекской и японской кухни.</p>
    <p>В 2015 году парк культуры и отдыха «им. Октябрьской Революции» занял 12-е место в общероссийском рейтинге лучших мест для отдыха и развлечений. ﻿В общий список вошли 553 парка, из которых 124 находятся в Москве.</p>


    <!--<div class=\"attractions__arrow-block\">-->

        <!--<div class=\"attractions__arrow\">-->
            <!--<img src=\"themes/main-theme/assets/images/arrow-right.svg\" class=\"attractions__arrow\">-->
        <!--</div>-->
    <!--</div>-->

    <!--";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            echo "-->

    <!--<div class=\"history-page standard-block\" id=\"history-block-";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "id", array()), "html", null, true);
            echo "\">-->
        <!--<div class=\"history-page__block\">-->
            <!--<div class=\"history-page__left-block__content\">-->
                <!--<div class=\"history-block__title\">";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "year", array()), "html", null, true);
            echo "</div>-->
                <!--<br>-->
                <!--<div>-->

                    <!--";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "content", array()), "html", null, true);
            echo "-->

                <!--</div>-->
            <!--</div>-->
        <!--</div>-->

        <!--<div class=\"history-page__block history-page__right-block\" style=\"background-image:url(' /storage/app/media/";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "image", array()), "html", null, true);
            echo "');\"></div>-->

        <!--<div style=\"clear:both;\"></div>-->

    <!--</div>-->

    <!--";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "-->




</div>
</div>



<!--<div class=\"standard-block history-linear-nav\">-->

   <!--<div class=\"history-linear-nav__line-through\"></div>-->


    <!--";
        // line 83
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            echo "-->

    <!--<div class=\"history-linear-nav__item-wrapper \" id=\"history-nav-item-";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "id", array()), "html", null, true);
            echo "\" pageId=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "id", array()), "html", null, true);
            echo "\">-->

        <!--<div class=\"history-linear-nav__item\">-->

            <!--<div class=\"history-linear-nav__item__year\">";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "year", array()), "html", null, true);
            echo "</div>-->

        <!--</div>-->

    <!--</div>-->

    <!--";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "-->

<!--</div>-->



<!--<script>-->

    <!--var idArray=[];-->
    <!--var i;-->
    <!--var active;-->


    <!--\$(function(){-->


        <!--";
        // line 111
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            echo "-->

        <!--idArray[idArray.length] = '";
            // line 113
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "id", array()), "html", null, true);
            echo "';-->

        <!--";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 115
        echo "-->

         <!--i = 0;-->

         <!--active=\$('#history-block-'+ idArray[i]);-->
        <!--active.addClass('history-page_active');-->
        <!--\$('#history-nav-item-'+idArray[i]).addClass('history-linear-nav__item-wrapper_active');-->
    <!--})-->

    <!--\$('.history-linear-nav__item-wrapper').click(function(){-->

        <!--\$('.history-linear-nav__item-wrapper_active').removeClass('history-linear-nav__item-wrapper_active');-->
        <!--\$(this).addClass('history-linear-nav__item-wrapper_active');-->

       <!--var pageId = \$(this).attr('pageId');-->
        <!--i = idArray.indexOf(pageId);-->
<!--//        \$('.attractions__arrow-block').trigger('click');-->
        <!--changePage();-->

        <!--if (i != idArray[idArray.length - 2]){-->
            <!--\$('.attractions__arrow-block').removeClass('attractions__arrow_not-active');-->
        <!--}-->

    <!--});-->


    <!--\$('.attractions__arrow-block').click(function arrowClick(){-->

        <!--i++;-->

        <!--if (i == idArray[idArray.length - 2]){-->
<!--//            \$('.attractions__arrow-block').addClass('attractions__arrow_not-active');-->
        <!--}-->

        <!--if (i == idArray[idArray.length - 1]){-->
            <!--i=0;-->
        <!--}-->

        <!--changePage();-->

        <!--\$('.history-linear-nav__item-wrapper_active').removeClass('history-linear-nav__item-wrapper_active');-->
        <!--\$('#history-nav-item-'+(i+1)).addClass('history-linear-nav__item-wrapper_active');-->


    <!--})-->

    <!--function changePage(){-->

        <!--active.removeClass('history-page_active');-->
        <!--active = \$('#history-block-'+ idArray[i]);-->
        <!--active.addClass('history-page_active');-->

    <!--}-->



<!--</script>-->";
    }

    public function getTemplateName()
    {
        return "/Users/nikita/WebstormProjects/park/themes/new-design/pages/history.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 115,  188 => 113,  181 => 111,  163 => 95,  150 => 89,  141 => 85,  134 => 83,  117 => 68,  104 => 62,  95 => 56,  88 => 52,  82 => 49,  75 => 47,  35 => 9,  33 => 8,  31 => 7,  29 => 6,  27 => 5,  25 => 4,  23 => 3,  19 => 1,);
    }
}
/* <div class="page-href" href="history"></div>*/
/* */
/* {% set records = builderList.records %}*/
/* {% set displayColumn = builderList.displayColumn %}*/
/* {% set noRecordsMessage = builderList.noRecordsMessage %}*/
/* {% set detailsPage = builderList.detailsPage %}*/
/* {% set detailsKeyColumn = builderList.detailsKeyColumn %}*/
/* {% set detailsUrlParameter = builderList.detailsUrlParameter %}*/
/* <div class="page-wrapper">*/
/* <div class="page__title text-align-center standard-block">ИСТОРИЯ ПАРКА</div>*/
/* */
/* */
/* <div class="history-page-wrapper standard-block">*/
/* */
/*     <p>Сначала на месте парка, напротив Управления СКЖД ближе к центру района находился огромный Софиевский собор с обширной Софиевской площадью. На площади проходили гуляния горожан. Тут же находилось и кладбище, где хоронили нахичеванских богачей. Ежегодно 23 апреля нахичеванские армяне в пестрых национальных костюмах заполняли площадь. Здесь проходили спортивные игры, выборы невест,можно было попробовать кулинарные изыски, приготовленные по семейным рецептам. Софиевский собор был разрушен в первые годы советской власти, посчитавшей его центром контрреволюции. Кладбище постигла та же участь, так как богатые усыпальницы живо напоминали о прошлой, достаточно благопристойной и сытой жизни. Сейчас территория бывших Софиевской площади и кладбища занята парком им. Октябрьской революции.</p>*/
/*     <p>Как культурная "единица" парк состоялся в 1926 году, площадь его составляла более 15 гектаров. Парк находился между городами Ростов-на-Дону и Нахичевань. В то время часть Пролетарского района Ростова-на-Дону была отдельным городом. В тот период  в Крымском ханстве армян стали убивать и притеснять, и они не смогли отстоять свои дома. Тогда армяне попросили помощи у русских и казаков, и те приютили их под стенами Ростовской крепости . Екатерина II  разрешила армянам- беженцам основать свой особый город Hop-Нахичевань, в результате того, что эти люди получили различные льготы и при этом не несли воинской повинности, город вскоре экономически расцвел и население быстро разрослось. Затем этот населённый пункт переименовали в Нахичевань-на-Дону , а в 1928 году его присоединили к Ростову-на-Дону.</p>*/
/*     <img class="history-page__photo" src="/themes/new-design/assets/images/history/information_items_property_3639.jpg">*/
/* */
/*     <p>В 1935 году было завершено строительство театра имени Горького, который является одним из шедевров мировой архитектуры﻿. В основу архитектурных форм здания была положена стилизация гусеничного трактора. Парк в плотную примыкает к театру. После постройки театра имени Горького, рядом с ним возвели красивейший фонтан  «Атланты».</p>*/
/*     <p>К 2000 году парк находился в полу-запущенном состоянии. Далеко не каждый житель Ростова-на-Дону решался зайти в него в темное время суток.</p>*/
/*     <p>Начиная с 2002 года парк Революции начали активно восстанавливать. В процессе реконструкции парка проводились работы по укладке новой тротуарной плитки, посадки деревьев и цветов, строительство детских площадок, установка аттракционов и памятников. За короткий промежуток времени парк Революции возродился вновь и стал очень популярен среди ростовчан и гостей нашего города как одно из самых лучших мест отдыха.</p>*/
/*     <p>Сегодня парк предоставляет своим посетителям разнообразные (в том числе бесплатные) аттракционы, зрелища, культурно-спортивные мероприятия. Огромная территория парка Революции засажена деревьями, кустарниками. Ко всему прочему в парке построено искусственное озеро для водоплавающих птиц, а также в птичниках содержатся павлины, журавли, лебеди и фламинго.</p>*/
/* */
/* */
/*     <img class="history-page__photo" src="/themes/new-design/assets/images/history/lJ2mva89PKQ.jpg">*/
/* */
/*     <p>На территории парка Революции есть спортивная зона. Активно отдохнуть можно на  катке "Ледоград". В парке есть  футбольное поле для мини-футбола и оборудована площадка для пейнтбола и экстремального лазанья по деревьям, а также площадка для настольного тенниса. На территории парка находится самая большая в Ростове-на-Дону воркаут площадка. На площадке ﻿ находится множество турников и перекладин, рукоходы и брусья, лестницы и скамьи для тренировки пресса. Все  оборудование современное и обеспечивает безопасные тренировки как детям, взрослым, так и лицам с ограниченными возможностями.</p>*/
/* */
/*     <p>В парке Революции установлен памятник Петра и Февронии, символизирующий семейное счастье, скамья примирения и другие интересные достопримечательности. Многие семейные пары приезжают на фотосессию в парк Революции.</p>*/
/*     <p>На территории парка есть много детских площадок, на которых детишки смогут поиграть, а родители отдохнуть в тенечке на лавочках, которые расположены на всех аллеях парка. И взрослым и детям будет весело и интересно в «Городке аттракционов», где можно найти развлечения на любой вкус. Там можно покататься на семейных, детских или экстремальных аттракционах.﻿</p>*/
/*     <p>На территории парка находится «Театр Папы Карло» ﻿- это первый в Ростове уличный кукольный театр. Представления в театре - бесплатные. Они рассчитаны на детей от 3-х лет. Репертуар театра постоянно обновляется.</p>*/
/* */
/* */
/*     <img class="history-page__photo" src="/themes/new-design/assets/images/history/k3.jpg">*/
/*     <p>Для любителей спокойного отдыха в парке есть зеленая зона, с ухоженными газонами и деревьями, на которых живут белочки. Белочки, уже привыкшие к людям, с нетерпением ждут угощений от посетителей парка. В парке вы сможете прокатиться на колесе обозрения. Его высота составляет 65 метров. ﻿Благодаря тому, что кабинки на колесе будут закрытыми, аттракцион будет работать не только летом, но и зимой. Желающим прокатиться на колесе, откроется прекрасный вид на реку Дон.</p>*/
/*     <p>На территории парка множество кафе и мест, где можно быстро и вкусно перекусить или утолить жажду. Одно из таких мест это ресторан «Ялла», в котором можно попробовать блюда восточной, европейской, итальянской, русской, узбекской и японской кухни.</p>*/
/*     <p>В 2015 году парк культуры и отдыха «им. Октябрьской Революции» занял 12-е место в общероссийском рейтинге лучших мест для отдыха и развлечений. ﻿В общий список вошли 553 парка, из которых 124 находятся в Москве.</p>*/
/* */
/* */
/*     <!--<div class="attractions__arrow-block">-->*/
/* */
/*         <!--<div class="attractions__arrow">-->*/
/*             <!--<img src="themes/main-theme/assets/images/arrow-right.svg" class="attractions__arrow">-->*/
/*         <!--</div>-->*/
/*     <!--</div>-->*/
/* */
/*     <!--{% for record in records %}-->*/
/* */
/*     <!--<div class="history-page standard-block" id="history-block-{{record.id}}">-->*/
/*         <!--<div class="history-page__block">-->*/
/*             <!--<div class="history-page__left-block__content">-->*/
/*                 <!--<div class="history-block__title">{{record.year}}</div>-->*/
/*                 <!--<br>-->*/
/*                 <!--<div>-->*/
/* */
/*                     <!--{{record.content}}-->*/
/* */
/*                 <!--</div>-->*/
/*             <!--</div>-->*/
/*         <!--</div>-->*/
/* */
/*         <!--<div class="history-page__block history-page__right-block" style="background-image:url(' /storage/app/media/{{record.image}}');"></div>-->*/
/* */
/*         <!--<div style="clear:both;"></div>-->*/
/* */
/*     <!--</div>-->*/
/* */
/*     <!--{% endfor %}-->*/
/* */
/* */
/* */
/* */
/* </div>*/
/* </div>*/
/* */
/* */
/* */
/* <!--<div class="standard-block history-linear-nav">-->*/
/* */
/*    <!--<div class="history-linear-nav__line-through"></div>-->*/
/* */
/* */
/*     <!--{% for record in records %}-->*/
/* */
/*     <!--<div class="history-linear-nav__item-wrapper " id="history-nav-item-{{record.id}}" pageId="{{record.id}}">-->*/
/* */
/*         <!--<div class="history-linear-nav__item">-->*/
/* */
/*             <!--<div class="history-linear-nav__item__year">{{record.year}}</div>-->*/
/* */
/*         <!--</div>-->*/
/* */
/*     <!--</div>-->*/
/* */
/*     <!--{% endfor %}-->*/
/* */
/* <!--</div>-->*/
/* */
/* */
/* */
/* <!--<script>-->*/
/* */
/*     <!--var idArray=[];-->*/
/*     <!--var i;-->*/
/*     <!--var active;-->*/
/* */
/* */
/*     <!--$(function(){-->*/
/* */
/* */
/*         <!--{% for record in records %}-->*/
/* */
/*         <!--idArray[idArray.length] = '{{record.id}}';-->*/
/* */
/*         <!--{% endfor %}-->*/
/* */
/*          <!--i = 0;-->*/
/* */
/*          <!--active=$('#history-block-'+ idArray[i]);-->*/
/*         <!--active.addClass('history-page_active');-->*/
/*         <!--$('#history-nav-item-'+idArray[i]).addClass('history-linear-nav__item-wrapper_active');-->*/
/*     <!--})-->*/
/* */
/*     <!--$('.history-linear-nav__item-wrapper').click(function(){-->*/
/* */
/*         <!--$('.history-linear-nav__item-wrapper_active').removeClass('history-linear-nav__item-wrapper_active');-->*/
/*         <!--$(this).addClass('history-linear-nav__item-wrapper_active');-->*/
/* */
/*        <!--var pageId = $(this).attr('pageId');-->*/
/*         <!--i = idArray.indexOf(pageId);-->*/
/* <!--//        $('.attractions__arrow-block').trigger('click');-->*/
/*         <!--changePage();-->*/
/* */
/*         <!--if (i != idArray[idArray.length - 2]){-->*/
/*             <!--$('.attractions__arrow-block').removeClass('attractions__arrow_not-active');-->*/
/*         <!--}-->*/
/* */
/*     <!--});-->*/
/* */
/* */
/*     <!--$('.attractions__arrow-block').click(function arrowClick(){-->*/
/* */
/*         <!--i++;-->*/
/* */
/*         <!--if (i == idArray[idArray.length - 2]){-->*/
/* <!--//            $('.attractions__arrow-block').addClass('attractions__arrow_not-active');-->*/
/*         <!--}-->*/
/* */
/*         <!--if (i == idArray[idArray.length - 1]){-->*/
/*             <!--i=0;-->*/
/*         <!--}-->*/
/* */
/*         <!--changePage();-->*/
/* */
/*         <!--$('.history-linear-nav__item-wrapper_active').removeClass('history-linear-nav__item-wrapper_active');-->*/
/*         <!--$('#history-nav-item-'+(i+1)).addClass('history-linear-nav__item-wrapper_active');-->*/
/* */
/* */
/*     <!--})-->*/
/* */
/*     <!--function changePage(){-->*/
/* */
/*         <!--active.removeClass('history-page_active');-->*/
/*         <!--active = $('#history-block-'+ idArray[i]);-->*/
/*         <!--active.addClass('history-page_active');-->*/
/* */
/*     <!--}-->*/
/* */
/* */
/* */
/* <!--</script>-->*/
