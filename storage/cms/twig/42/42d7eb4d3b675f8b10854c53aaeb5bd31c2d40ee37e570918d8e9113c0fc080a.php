<?php

/* /Users/nikita/WebstormProjects/park/themes/new-design/pages/index.htm */
class __TwigTemplate_7ab9fed485f58a2d35cd2f5f550774c7b675deb31dda6e8bd0c5596fbe921e61 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"page-wrapper\">

";
        // line 3
        $context["records"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "records", array());
        // line 4
        $context["displayColumn"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "displayColumn", array());
        // line 5
        $context["noRecordsMessage"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "noRecordsMessage", array());
        // line 6
        $context["detailsPage"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "detailsPage", array());
        // line 7
        $context["detailsKeyColumn"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "detailsKeyColumn", array());
        // line 8
        $context["detailsUrlParameter"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "detailsUrlParameter", array());
        // line 9
        echo "
    <div class=\"slider\">

        ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 13
            echo "        <img src=\"/storage/app/media/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "image", array()), "html", null, true);
            echo "\" class=\"slider__item\">

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "
    </div>


    <script type=\"text/javascript\" src=\"/themes/new-design/assets/sources/slick-master/slick/slick.js\"></script>
    <script>

        \$(\".slider\").slick({
            autoplay: true
        });

    </script>

    <div class=\"standard-block page__links\">

        <a href=\"/aboutpark\">
            <div class=\"page__link\">

                <img src=\"/themes/new-design/assets/images/link_aboutpark.svg\">
                <br>
                О ПАРКЕ

            </div>
        </a>

        <a href=\"/news\">
            <div class=\"page__link page__link_grey\">

                <img src=\"/themes/new-design/assets/images/link_news.svg\">
                <br>
                НОВОСТИ
            </div>
        </a>

        <a href=\"/attractions\">
            <div class=\"page__link\">

                <img src=\"/themes/new-design/assets/images/link_attractions.svg\">
                <br>
                АТТРАКЦИОНЫ
            </div>
        </a>

        <a href=\"/places\">
            <div class=\"page__link\">

                <img src=\"/themes/new-design/assets/images/link_places.svg\">
                <br>
                МЕСТА
            </div>
        </a>


    </div>


    <div class=\"standard-block page__title\">МЕРОПРИЯТИЯ И АКЦИИ</div>

    <div class=\"standard-block events\">

        ";
        // line 76
        $context["records"] = $this->getAttribute((isset($context["events"]) ? $context["events"] : null), "records", array());
        // line 77
        echo "        ";
        $context["displayColumn"] = $this->getAttribute((isset($context["events"]) ? $context["events"] : null), "displayColumn", array());
        // line 78
        echo "        ";
        $context["noRecordsMessage"] = $this->getAttribute((isset($context["events"]) ? $context["events"] : null), "noRecordsMessage", array());
        // line 79
        echo "        ";
        $context["detailsPage"] = $this->getAttribute((isset($context["events"]) ? $context["events"] : null), "detailsPage", array());
        // line 80
        echo "        ";
        $context["detailsKeyColumn"] = $this->getAttribute((isset($context["events"]) ? $context["events"] : null), "detailsKeyColumn", array());
        // line 81
        echo "        ";
        $context["detailsUrlParameter"] = $this->getAttribute((isset($context["events"]) ? $context["events"] : null), "detailsUrlParameter", array());
        // line 82
        echo "


        ";
        // line 85
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["event"]) {
            // line 86
            echo "
        <div class=\"events__item\" style=\"background-image: url('/storage/app/media/";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "image", array()), "html", null, true);
            echo "')\">

            <div class=\"events__item__title\">

                ";
            // line 91
            echo $this->getAttribute($context["event"], "title", array());
            echo "

            </div>
            <div class=\"events__item__description\">
                ";
            // line 95
            echo $this->getAttribute($context["event"], "content", array());
            echo "
            </div>
            <a href=\"/event/";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "id", array()), "html", null, true);
            echo "\"><div class=\"events__item__link\">ЧИТАТЬ ДАЛЕЕ</div></a>
           ";
            // line 98
            if ($this->getAttribute($context["event"], "title", array())) {
                echo " <div class=\"events__item__opacity\"></div> ";
            }
            // line 99
            echo "        </div>


        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['event'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 103
        echo "

    </div>

    <script src=\"/themes/new-design/assets/sources/jTruncate.js\"></script>

    <script>
        \$('.events__item__description').jTruncate(  {
                    length:80
                }
        );
    </script>


    <div class=\"bottom-marker\">

        <img src=\"/themes/new-design/assets/images/bottom-marker.jpg\">


    </div>

    <div class=\"commercial standard-block\">

        <div class=\"commercial__title\">РАЗМЕЩЕНИЕ РЕКЛАМЫ В <span class=\"color-green\">ПАРКЕ</span></div>

        <div class=\"commercial__text\">
        Вы можете разместить свою рекламу на территории парка.<br>
        Предлагаем Вам использовать возможности парка: подпроведение корпоративных мероприятий, праздников, промо-мероприятий,
        размещение рекламного баннера.<br><br>

            К Вашим услугам:<br>
            <ul class=\"commercial__ul\">
           <li> - Городок аттракционов открыт с апреля по октябрь</li>
           <li> - Кукольный театр под открытым небом</li>
            <li>- Возможность аренды сцен для проведения мероприятий различного масштаба.</li>
           <li> - Возможность проведения промо-акций, презентаций на территории парка;</li>
            <li>- Уникальная возможность провести День Рождения вашего ребенка на открытом воздухе;</li>
            <li>- Удобное расположение в центре города (Театральная площадь) с парковкой;</li>
            <li>- На корпоративные привозы действуют скидки.</li>
            </ul>

            <div class=\"commercial__small-text\">По всем вопросам обращайтесь по номеру: 263-53-09, 8-904-343-92-18<br>
            Пишите на почту: ﻿ ali-kovalen@yandex.ru
            </div>
        </div>



    </div>



</div>";
    }

    public function getTemplateName()
    {
        return "/Users/nikita/WebstormProjects/park/themes/new-design/pages/index.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 103,  172 => 99,  168 => 98,  164 => 97,  159 => 95,  152 => 91,  145 => 87,  142 => 86,  138 => 85,  133 => 82,  130 => 81,  127 => 80,  124 => 79,  121 => 78,  118 => 77,  116 => 76,  54 => 16,  44 => 13,  40 => 12,  35 => 9,  33 => 8,  31 => 7,  29 => 6,  27 => 5,  25 => 4,  23 => 3,  19 => 1,);
    }
}
/* <div class="page-wrapper">*/
/* */
/* {% set records = builderList.records %}*/
/* {% set displayColumn = builderList.displayColumn %}*/
/* {% set noRecordsMessage = builderList.noRecordsMessage %}*/
/* {% set detailsPage = builderList.detailsPage %}*/
/* {% set detailsKeyColumn = builderList.detailsKeyColumn %}*/
/* {% set detailsUrlParameter = builderList.detailsUrlParameter %}*/
/* */
/*     <div class="slider">*/
/* */
/*         {% for record in records %}*/
/*         <img src="/storage/app/media/{{record.image}}" class="slider__item">*/
/* */
/*         {% endfor %}*/
/* */
/*     </div>*/
/* */
/* */
/*     <script type="text/javascript" src="/themes/new-design/assets/sources/slick-master/slick/slick.js"></script>*/
/*     <script>*/
/* */
/*         $(".slider").slick({*/
/*             autoplay: true*/
/*         });*/
/* */
/*     </script>*/
/* */
/*     <div class="standard-block page__links">*/
/* */
/*         <a href="/aboutpark">*/
/*             <div class="page__link">*/
/* */
/*                 <img src="/themes/new-design/assets/images/link_aboutpark.svg">*/
/*                 <br>*/
/*                 О ПАРКЕ*/
/* */
/*             </div>*/
/*         </a>*/
/* */
/*         <a href="/news">*/
/*             <div class="page__link page__link_grey">*/
/* */
/*                 <img src="/themes/new-design/assets/images/link_news.svg">*/
/*                 <br>*/
/*                 НОВОСТИ*/
/*             </div>*/
/*         </a>*/
/* */
/*         <a href="/attractions">*/
/*             <div class="page__link">*/
/* */
/*                 <img src="/themes/new-design/assets/images/link_attractions.svg">*/
/*                 <br>*/
/*                 АТТРАКЦИОНЫ*/
/*             </div>*/
/*         </a>*/
/* */
/*         <a href="/places">*/
/*             <div class="page__link">*/
/* */
/*                 <img src="/themes/new-design/assets/images/link_places.svg">*/
/*                 <br>*/
/*                 МЕСТА*/
/*             </div>*/
/*         </a>*/
/* */
/* */
/*     </div>*/
/* */
/* */
/*     <div class="standard-block page__title">МЕРОПРИЯТИЯ И АКЦИИ</div>*/
/* */
/*     <div class="standard-block events">*/
/* */
/*         {% set records = events.records %}*/
/*         {% set displayColumn = events.displayColumn %}*/
/*         {% set noRecordsMessage = events.noRecordsMessage %}*/
/*         {% set detailsPage = events.detailsPage %}*/
/*         {% set detailsKeyColumn = events.detailsKeyColumn %}*/
/*         {% set detailsUrlParameter = events.detailsUrlParameter %}*/
/* */
/* */
/* */
/*         {% for event in records %}*/
/* */
/*         <div class="events__item" style="background-image: url('/storage/app/media/{{event.image}}')">*/
/* */
/*             <div class="events__item__title">*/
/* */
/*                 {{event.title | raw}}*/
/* */
/*             </div>*/
/*             <div class="events__item__description">*/
/*                 {{event.content | raw}}*/
/*             </div>*/
/*             <a href="/event/{{event.id}}"><div class="events__item__link">ЧИТАТЬ ДАЛЕЕ</div></a>*/
/*            {% if event.title %} <div class="events__item__opacity"></div> {% endif %}*/
/*         </div>*/
/* */
/* */
/*         {% endfor %}*/
/* */
/* */
/*     </div>*/
/* */
/*     <script src="/themes/new-design/assets/sources/jTruncate.js"></script>*/
/* */
/*     <script>*/
/*         $('.events__item__description').jTruncate(  {*/
/*                     length:80*/
/*                 }*/
/*         );*/
/*     </script>*/
/* */
/* */
/*     <div class="bottom-marker">*/
/* */
/*         <img src="/themes/new-design/assets/images/bottom-marker.jpg">*/
/* */
/* */
/*     </div>*/
/* */
/*     <div class="commercial standard-block">*/
/* */
/*         <div class="commercial__title">РАЗМЕЩЕНИЕ РЕКЛАМЫ В <span class="color-green">ПАРКЕ</span></div>*/
/* */
/*         <div class="commercial__text">*/
/*         Вы можете разместить свою рекламу на территории парка.<br>*/
/*         Предлагаем Вам использовать возможности парка: подпроведение корпоративных мероприятий, праздников, промо-мероприятий,*/
/*         размещение рекламного баннера.<br><br>*/
/* */
/*             К Вашим услугам:<br>*/
/*             <ul class="commercial__ul">*/
/*            <li> - Городок аттракционов открыт с апреля по октябрь</li>*/
/*            <li> - Кукольный театр под открытым небом</li>*/
/*             <li>- Возможность аренды сцен для проведения мероприятий различного масштаба.</li>*/
/*            <li> - Возможность проведения промо-акций, презентаций на территории парка;</li>*/
/*             <li>- Уникальная возможность провести День Рождения вашего ребенка на открытом воздухе;</li>*/
/*             <li>- Удобное расположение в центре города (Театральная площадь) с парковкой;</li>*/
/*             <li>- На корпоративные привозы действуют скидки.</li>*/
/*             </ul>*/
/* */
/*             <div class="commercial__small-text">По всем вопросам обращайтесь по номеру: 263-53-09, 8-904-343-92-18<br>*/
/*             Пишите на почту: ﻿ ali-kovalen@yandex.ru*/
/*             </div>*/
/*         </div>*/
/* */
/* */
/* */
/*     </div>*/
/* */
/* */
/* */
/* </div>*/
