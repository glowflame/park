<?php

/* /Users/nikita/WebstormProjects/park/themes/new-design/pages/faq.htm */
class __TwigTemplate_f8c19dbc5d73f3d8459c43ed66864ac131be6a5b3b22a6a5a46341c823960a9f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"standard-block page__title  text-align-center\">ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ</div>
<div class=\"page-wrapper\">

";
        // line 4
        $context["records"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "records", array());
        // line 5
        $context["displayColumn"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "displayColumn", array());
        // line 6
        $context["noRecordsMessage"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "noRecordsMessage", array());
        // line 7
        $context["detailsPage"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "detailsPage", array());
        // line 8
        $context["detailsKeyColumn"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "detailsKeyColumn", array());
        // line 9
        $context["detailsUrlParameter"] = $this->getAttribute((isset($context["builderList"]) ? $context["builderList"] : null), "detailsUrlParameter", array());
        // line 10
        echo "
\t<div class=\"standard-block \">

\t";
        // line 13
        $context["i"] = 0;
        // line 14
        echo "
\t\t<div class=\"faq_column\">
";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 17
            echo "\t\t\t";
            if ((((isset($context["i"]) ? $context["i"] : null) % 2) == 0)) {
                // line 18
                echo "\t<div class=\"faq-block\"  style=\"float:right;\" >

\t\t<div class=\"faq__arrow-block-wrapper\">
\t\t\t<div class=\"faq__arrow-block faq-open\" to=\"";
                // line 21
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "id", array()), "html", null, true);
                echo "\" id=\"arrow-";
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "id", array()), "html", null, true);
                echo "\"></div>
\t\t</div>

\t\t<div class=\"faq__content\">
\t\t\t<div class=\"faq__title faq-open\" to=\"";
                // line 25
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t";
                // line 26
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "title", array()), "html", null, true);
                echo "
\t\t\t</div>

\t\t\t<div class=\"faq__body\" id=\"body-";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t";
                // line 30
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "content", array()), "html", null, true);
                echo "
\t\t\t</div>
\t\t</div>
\t</div>
\t\t\t";
            }
            // line 35
            echo "\t";
            $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
            // line 36
            echo "
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "\t\t</div>
\t\t";
        // line 39
        $context["i"] = 0;
        // line 40
        echo "
\t\t<div class=\"faq_column\">

\t\t\t";
        // line 43
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 44
            echo "\t\t\t";
            if ((((isset($context["i"]) ? $context["i"] : null) % 2) == 1)) {
                // line 45
                echo "\t\t\t<div class=\"faq-block\"  style=\"float:right;\" >

\t\t\t\t<div class=\"faq__arrow-block-wrapper\">
\t\t\t\t\t<div class=\"faq__arrow-block faq-open\" to=\"";
                // line 48
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "id", array()), "html", null, true);
                echo "\" id=\"arrow-";
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "id", array()), "html", null, true);
                echo "\"></div>
\t\t\t\t</div>

\t\t\t\t<div class=\"faq__content\">
\t\t\t\t\t<div class=\"faq__title faq-open\" to=\"";
                // line 52
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t\t\t";
                // line 53
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "title", array()), "html", null, true);
                echo "
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"faq__body\" id=\"body-";
                // line 56
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t\t\t";
                // line 57
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "content", array()), "html", null, true);
                echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t";
            }
            // line 62
            echo "\t\t\t";
            $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
            // line 63
            echo "
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "

\t\t</div>


\t<div style=\"clear:both;\"></div>

</div>

</div>

<script>

\t\$('.faq-open').click(function(){

\t\t\$('#body-'+\$(this).attr('to')).slideToggle({
\t\t\t\tduration:200
\t\t});

\t\t\$('#arrow-'+\$(this).attr('to')).toggleClass(\"faq__arrow-block_active\");
\t})

</script>";
    }

    public function getTemplateName()
    {
        return "/Users/nikita/WebstormProjects/park/themes/new-design/pages/faq.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 65,  156 => 63,  153 => 62,  145 => 57,  141 => 56,  135 => 53,  131 => 52,  122 => 48,  117 => 45,  114 => 44,  110 => 43,  105 => 40,  103 => 39,  100 => 38,  93 => 36,  90 => 35,  82 => 30,  78 => 29,  72 => 26,  68 => 25,  59 => 21,  54 => 18,  51 => 17,  47 => 16,  43 => 14,  41 => 13,  36 => 10,  34 => 9,  32 => 8,  30 => 7,  28 => 6,  26 => 5,  24 => 4,  19 => 1,);
    }
}
/* <div class="standard-block page__title  text-align-center">ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ</div>*/
/* <div class="page-wrapper">*/
/* */
/* {% set records = builderList.records %}*/
/* {% set displayColumn = builderList.displayColumn %}*/
/* {% set noRecordsMessage = builderList.noRecordsMessage %}*/
/* {% set detailsPage = builderList.detailsPage %}*/
/* {% set detailsKeyColumn = builderList.detailsKeyColumn %}*/
/* {% set detailsUrlParameter = builderList.detailsUrlParameter %}*/
/* */
/* 	<div class="standard-block ">*/
/* */
/* 	{% set i=0 %}*/
/* */
/* 		<div class="faq_column">*/
/* {% for record in records %}*/
/* 			{% if i%2 == 0 %}*/
/* 	<div class="faq-block"  style="float:right;" >*/
/* */
/* 		<div class="faq__arrow-block-wrapper">*/
/* 			<div class="faq__arrow-block faq-open" to="{{record.id}}" id="arrow-{{record.id}}"></div>*/
/* 		</div>*/
/* */
/* 		<div class="faq__content">*/
/* 			<div class="faq__title faq-open" to="{{record.id}}">*/
/* 				{{record.title}}*/
/* 			</div>*/
/* */
/* 			<div class="faq__body" id="body-{{record.id}}">*/
/* 				{{record.content}}*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 			{% endif %}*/
/* 	{% set i=i+1 %}*/
/* */
/* {% endfor %}*/
/* 		</div>*/
/* 		{% set i=0 %}*/
/* */
/* 		<div class="faq_column">*/
/* */
/* 			{% for record in records %}*/
/* 			{% if i%2 == 1 %}*/
/* 			<div class="faq-block"  style="float:right;" >*/
/* */
/* 				<div class="faq__arrow-block-wrapper">*/
/* 					<div class="faq__arrow-block faq-open" to="{{record.id}}" id="arrow-{{record.id}}"></div>*/
/* 				</div>*/
/* */
/* 				<div class="faq__content">*/
/* 					<div class="faq__title faq-open" to="{{record.id}}">*/
/* 						{{record.title}}*/
/* 					</div>*/
/* */
/* 					<div class="faq__body" id="body-{{record.id}}">*/
/* 						{{record.content}}*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 			{% endif %}*/
/* 			{% set i=i+1 %}*/
/* */
/* 			{% endfor %}*/
/* */
/* */
/* 		</div>*/
/* */
/* */
/* 	<div style="clear:both;"></div>*/
/* */
/* </div>*/
/* */
/* </div>*/
/* */
/* <script>*/
/* */
/* 	$('.faq-open').click(function(){*/
/* */
/* 		$('#body-'+$(this).attr('to')).slideToggle({*/
/* 				duration:200*/
/* 		});*/
/* */
/* 		$('#arrow-'+$(this).attr('to')).toggleClass("faq__arrow-block_active");*/
/* 	})*/
/* */
/* </script>*/
