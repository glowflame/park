<?php

/* /Users/nikita/WebstormProjects/park/themes/new-design/pages/news.htm */
class __TwigTemplate_2df89bf901d314813e9750764ed0ea3d6e8d91ebbc8900fced54a9fa38f60251 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"page-wrapper\">
    <div class=\"standard-block page__title\">
        НОВОСТИ
    </div>

    <div class=\"news standard-block\">

        ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) ? $context["posts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 9
            echo "
        <div class=\"new-block\">
            <div class=\"new-block__img\" style=\"background-image:url('/storage/app/media";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "image", array()), "html", null, true);
            echo "');\"></div>
            <!--<img src='/storage/app/media";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "image", array()), "html", null, true);
            echo "'>-->
            <div class=\"new-block__text\">
                <div class=\"title new-block__title\">";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
            echo "</div>
                <div class=\"new-crop\">";
            // line 15
            echo $this->getAttribute($context["post"], "content", array());
            echo "</div>
                <a href=\"new/";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "id", array()), "html", null, true);
            echo "\">
                    <div class=\"new__link\">ЧИТАТЬ ДАЛЕЕ</div>
                </a>
            </div>
        </div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "
        <div style=\"clear:both;\"></div>
    </div>
</div>

<script src=\"/themes/new-design/assets/sources/jTruncate.js\"></script>

<script>
    \$('.new-crop').jTruncate({
                length: 180
            }
    );
</script>";
    }

    public function getTemplateName()
    {
        return "/Users/nikita/WebstormProjects/park/themes/new-design/pages/news.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 23,  53 => 16,  49 => 15,  45 => 14,  40 => 12,  36 => 11,  32 => 9,  28 => 8,  19 => 1,);
    }
}
/* <div class="page-wrapper">*/
/*     <div class="standard-block page__title">*/
/*         НОВОСТИ*/
/*     </div>*/
/* */
/*     <div class="news standard-block">*/
/* */
/*         {% for post in posts %}*/
/* */
/*         <div class="new-block">*/
/*             <div class="new-block__img" style="background-image:url('/storage/app/media{{ post.image }}');"></div>*/
/*             <!--<img src='/storage/app/media{{ post.image }}'>-->*/
/*             <div class="new-block__text">*/
/*                 <div class="title new-block__title">{{ post.title }}</div>*/
/*                 <div class="new-crop">{{ post.content|raw }}</div>*/
/*                 <a href="new/{{post.id}}">*/
/*                     <div class="new__link">ЧИТАТЬ ДАЛЕЕ</div>*/
/*                 </a>*/
/*             </div>*/
/*         </div>*/
/* */
/*         {% endfor %}*/
/* */
/*         <div style="clear:both;"></div>*/
/*     </div>*/
/* </div>*/
/* */
/* <script src="/themes/new-design/assets/sources/jTruncate.js"></script>*/
/* */
/* <script>*/
/*     $('.new-crop').jTruncate({*/
/*                 length: 180*/
/*             }*/
/*     );*/
/* </script>*/
