<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNikitaContentSpots extends Migration
{
    public function up()
    {
        Schema::table('nikita_content_spots', function($table)
        {
            $table->text('description')->nullable();
            $table->string('image', 255)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('nikita_content_spots', function($table)
        {
            $table->dropColumn('description');
            $table->string('image', 255)->nullable(false)->change();
        });
    }
}
