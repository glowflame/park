<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNikitaContentHistory extends Migration
{
    public function up()
    {
        Schema::table('nikita_content_history', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('nikita_content_history', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
}
