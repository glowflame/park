<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNikitaContentHistory2 extends Migration
{
    public function up()
    {
        Schema::table('nikita_content_history', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
            $table->string('year')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('nikita_content_history', function($table)
        {
            $table->increments('id')->unsigned()->change();
            $table->string('year', 255)->nullable(false)->change();
        });
    }
}
