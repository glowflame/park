<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteNikitaContentAboutpark extends Migration
{
    public function up()
    {
        Schema::dropIfExists('nikita_content_aboutpark');
    }
    
    public function down()
    {
        Schema::create('nikita_content_aboutpark', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('title', 255);
            $table->string('content', 255);
            $table->string('image', 255);
        });
    }
}
