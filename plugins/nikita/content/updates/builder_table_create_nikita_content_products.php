<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNikitaContentProducts extends Migration
{
    public function up()
    {
        Schema::create('nikita_content_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('content');
            $table->string('image');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nikita_content_products');
    }
}
