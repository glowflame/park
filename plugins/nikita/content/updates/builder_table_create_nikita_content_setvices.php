<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNikitaContentSetvices extends Migration
{
    public function up()
    {
        Schema::create('nikita_content_setvices', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('image')->nullable();
            $table->string('content')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nikita_content_setvices');
    }
}
