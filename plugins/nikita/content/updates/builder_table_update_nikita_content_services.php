<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNikitaContentServices extends Migration
{
    public function up()
    {
        Schema::rename('nikita_content_setvices', 'nikita_content_services');
        Schema::table('nikita_content_services', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::rename('nikita_content_services', 'nikita_content_setvices');
        Schema::table('nikita_content_setvices', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
}
