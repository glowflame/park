<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNikitaContentEvents3 extends Migration
{
    public function up()
    {
        Schema::table('nikita_content_events', function($table)
        {
            $table->date('date')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('nikita_content_events', function($table)
        {
            $table->date('date')->nullable(false)->change();
        });
    }
}
