<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNikitaContentEvents extends Migration
{
    public function up()
    {
        Schema::create('nikita_content_events', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->dateTime('date');
            $table->string('content')->nullable();
            $table->integer('image')->nullable();
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nikita_content_events');
    }
}
