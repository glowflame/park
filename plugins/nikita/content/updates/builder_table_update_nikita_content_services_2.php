<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNikitaContentServices2 extends Migration
{
    public function up()
    {
        Schema::rename('nikita_content_', 'nikita_content_services');
        Schema::table('nikita_content_services', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::rename('nikita_content_services', 'nikita_content_');
        Schema::table('nikita_content_', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
}
