<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteNikitaContentHistory extends Migration
{
    public function up()
    {
        Schema::dropIfExists('nikita_content_history');
    }
    
    public function down()
    {
        Schema::create('nikita_content_history', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('year', 255);
            $table->string('content', 255)->nullable();
            $table->string('image', 255)->nullable();
        });
    }
}
