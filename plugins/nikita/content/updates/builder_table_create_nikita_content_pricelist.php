<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNikitaContentPricelist extends Migration
{
    public function up()
    {
        Schema::create('nikita_content_pricelist', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('weekdays')->nullable();
            $table->string('weekend')->nullable();
            $table->string('holidays')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nikita_content_pricelist');
    }
}
