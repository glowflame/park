<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNikitaContentEvents2 extends Migration
{
    public function up()
    {
        Schema::table('nikita_content_events', function($table)
        {
            $table->string('image')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('nikita_content_events', function($table)
        {
            $table->integer('image')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
