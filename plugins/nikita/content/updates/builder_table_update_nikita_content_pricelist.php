<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNikitaContentPricelist extends Migration
{
    public function up()
    {
        Schema::table('nikita_content_pricelist', function($table)
        {
            $table->string('hours')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('nikita_content_pricelist', function($table)
        {

        });
    }
}
