<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNikitaContentEvents extends Migration
{
    public function up()
    {
        Schema::table('nikita_content_events', function($table)
        {
            $table->date('date')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('nikita_content_events', function($table)
        {
            $table->dateTime('date')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
