<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNikitaContentAboutpark extends Migration
{
    public function up()
    {
        Schema::create('nikita_content_aboutpark', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('title');
            $table->string('content');
            $table->string('image');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nikita_content_aboutpark');
    }
}
