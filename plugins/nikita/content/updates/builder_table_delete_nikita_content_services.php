<?php namespace Nikita\Content\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteNikitaContentServices extends Migration
{
    public function up()
    {
        Schema::dropIfExists('nikita_content_services');
    }
    
    public function down()
    {
        Schema::create('nikita_content_services', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 255);
            $table->string('slug', 255);
            $table->string('image', 255)->nullable();
            $table->string('content', 255)->nullable();
        });
    }
}
