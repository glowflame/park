<?php namespace Nikita\Content\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Attractions extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'attractions' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Nikita.Content', 'content', 'attractions');
    }
}